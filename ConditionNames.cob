       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONDITIONNAMES.
       AUTHOR. Kittipon Thaweelarb.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 CHARIN             PIC X.
          88 VOWEL           VALUE "a", "e", "i", "o", "u".
          88 CONSONANT       VALUE "b", "c", "f", "f", "g", "h"
                              "j" THRU "n", "p" THRU "t", "V" THRU "z".
          88 DIGIT           VALUE "O" THRU "9".
          88 VALIDCHARACTER  VALUE "a" THRU "2", "O" THRU "9".

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY 
           "Enter lower case character or digit. Invalid char ends."
           ACCEPT CHARIN
           PERFORM UNTIL NOT VALIDCHARACTER
              EVALUATE TRUE
                   WHEN VOWEL
                        DISPLAY "The letter " CHARIN " is a vowel."
                   WHEN CONSONANT
                        DISPLAY"The letter " CHARIN " is a consonant."
                   WHEN DIGIT
                        DISPLAY CHARIN " is a digit."
              END-EVALUATE
              ACCEPT CHARIN
           END-PERFORM
           GOBACK.