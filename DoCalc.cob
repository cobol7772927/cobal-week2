       IDENTIFICATION DIVISION.
       PROGRAM-ID. DOCALC.
       AUTHOR. Kittipon Thaweelarb.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 FIRSTNUM    PIC 9     VALUE ZEROS. 
       01 SECONDNUM   PIC 9     VALUE ZEROS. 
       01 CALCRESULT  PIC 99    VALUE 0.
       01 USERPROMT   PIC X(38) VALUE
             "Please enter two single digit numbers".
                        
       PROCEDURE DIVISION.
       CALCUATERESULT.
           DISPLAY USERPROMT
           ACCEPT FIRSTNUM
           ACCEPT SECONDNUM 
           COMPUTE CALCRESULT = FIRSTNUM + SECONDNUM 
           DISPLAY "Result is = ", CALCRESULT
           GOBACK.